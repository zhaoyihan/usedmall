const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  publicPath: '/',
  devServer: {
    port: 8000,
    open: true,
    proxy: {
      '/api': {
        target: 'http://localhost:8080', // 设置你的后端服务地址
        pathRewrite: {
          '^/api': '',
        },
      },
    },
  },
})
