// 封装的axios请求工具类
// YiHan

import axios from 'axios';
import {Message, MessageBox} from "element-ui";

const service = axios.create({
    baseURL: process.env.VUE_APP_BASE_API, // 这里可以根据实际情况设置基础路径
    timeout: 60000, // 请求超时时间
});

// 请求拦截器
service.interceptors.request.use(
    config => {
        // 可以在这里设置请求头等
        return config;
    },
    error => {
        return Promise.reject(error);
    }
);

// 响应拦截器
service.interceptors.response.use(
    response => {
        const res = response.data;
        if (res.code === 200) {
            return res
        } else if (res.code === 500) {
            Message({
                message: res.msg,
                type: 'error',
                duration: 5 * 1000
            })
        }
    },
    error => {
        var errorStatus = error.response.code;
        var errorData = error.response.data;
        var messageTxt = "";
        if (errorStatus != 200) {
            messageTxt = "服务器内部错误，请联系管理员";
        } else {
            messageTxt = '失败原因：' + errorData.code + '--' + errorData.repMsg;
        }
        Message({
            message: messageTxt,
            type: 'error',
            duration: 5 * 1000
        })
    }
)

export default service;
