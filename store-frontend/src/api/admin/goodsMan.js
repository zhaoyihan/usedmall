// 管理员商品的接口
// YiHan
import request from '@/utils/request'

// 查询商品统计信息
export function queryGoodsDate() {
  return request({
    url: '/admin/goods/getDate',
    method: 'get'
  })
}

export function queryUploadNum(){
  return request({
    url: '/admin/goods/getUploadNum',
    method: 'get'
  })
}

export function orderList(){
  return request({
    url: '/admin/goods/list',
    method: 'get'
  })
}

export function queryList(query){
  return request({
    url: '/admin/goods/getList',
    method: 'get',
    params: query
  })
}

export function exportList(query){
  return request({
    url: '',
    method: '',
    params: query
  })
}

export function downGoods(goodsId){
  return request({
    url: '/admin/goods/down/' + goodsId,
    method: 'get'
  })
}
