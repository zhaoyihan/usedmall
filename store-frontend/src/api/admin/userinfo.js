// 用户的接口
// YiHan
import request from '@/utils/request';

export function getUserInfoList(params) {
    return request({
        url: '/admin/userinfo/list',
        method: 'get',
        params: params
    });
}

export function deleteUser(userId) {
    return request({
        url: '/admin/userinfo/del/' + userId,
        method: 'get'
    });
}

// Add more API methods as needed for user_info management
