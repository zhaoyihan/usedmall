// 短信的接口
// YiHan
import request from '@/utils/request'

export function sendMsg(data){
    return request({
        url: '/user/msg/send',
        method: 'post',
        data: data
    })
}

export function getMsg(query){
    return request({
        url: '/user/msg/getMsg',
        method: 'get',
        params: query
    })
}
