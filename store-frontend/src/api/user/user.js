// 商品接口
// YiHan

import request from '@/utils/request';


// 登录
export function login(data, loginType){
    return request({
        url: '/user/login/' + loginType,
        method: 'post',
        data: data
    })
}

//　发送验证码
export function getPhoneCode(phone){
    return request({
        url: '/user/getPhoneCode/' + phone,
        method: 'get'
    })
}

// 校验手机号以及验证码
export function checkPhoneCode(data){
    return request({
        url: '/user/checkPhoneNum',
        method: 'post',
        data: data
    })
}

// 验证码确认之后 输入用户名密码 更新其信息
export function updateUserInfo(data){
    return request({
        url: '/user/updateUserInfo',
        method: 'post',
        data: data
    })
}

// 获取用户信息
export function getUserInfo(userId){
    return request({
        url: '/user/getUserInfo/' + userId,
        method: 'get'
    })
}

export function saveUserInfo(data){
    return request({
        url: '/user/saveUserInfo',
        method: 'post',
        data: data
    })
}
