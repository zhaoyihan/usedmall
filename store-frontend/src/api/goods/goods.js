// 管理员对于商品的接口
// YiHan

import request from '@/utils/request'

// 上传excel表格的数据
// 文件上传
export function upload(query, data) {
    return request({
        url: '/goods/upload',
        method: 'post',
        headers: { 'Content-Type': 'multipart/form-data' },
        params: query,
        data: data
    });
}

export function addGoods(data){
    return request({
        url: '/goods/add',
        method: 'post',
        data: data
    })
}

export function delGoods(goodsId){
    return request({
        url: '/goods/del/' + goodsId ,
        method: 'get'
    })
}
