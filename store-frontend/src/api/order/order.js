// 订单接口
// YiHan

import request from '@/utils/request'

export function addOrder(data){
    return request({
        url: '/order/add',
        method: 'post',
        data: data
    })
}


export function getOrderList(userAccount, type){
    return request({
        url: '/order/getOrderList/' + userAccount +'/' + type,
        method: 'get'
    })
}

export function delOrder(id){
    return request({
        url: '/order/del/' + id,
        method: 'get'
    })
}
