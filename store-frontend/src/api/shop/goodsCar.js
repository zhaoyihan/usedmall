// 购物车接口
// YiHan

import request from '@/utils/request';


// 查询购物车列表
export function getCartList(userId){
    return request({
        url: '/goodsCar/getGoodsList/' + userId,
        method: 'get',
    })
}

// 走购物车删除
export function deleteFromCart(data){
    return request({
        url: '/goodsCar/deleteFromCart',
        method: 'post',
        data: data
    })
}

// 加购物车
export function addGoodsCart(data){
    return request({
        url: '/goodsCar/add',
        method: 'post',
        data: data
    })
}
