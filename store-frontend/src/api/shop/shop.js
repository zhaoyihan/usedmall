// 商品接口
// YiHan

import request from '@/utils/request';

// 轮播图
export function getList(){
    return request({
        url: '/goods/getPhotoList',
        method: 'get'
    })
}

// 商品详情
export function getGoodInfoDetail(goodsId){
    return request({
        url: '/goods/getGoodInfo/' + goodsId,
        method: 'get'
    })
}

// 评论列表
export function addGoodsContent(data){
    return request({
        url: '/goods/addGoodsContent',
        method: 'post',
        data: data
    })
}

// 查询推荐商品
export function getRecommendList(){
    return request({
        url: '/goods/getRecommendList',
        method: 'get'
    })
}

export function getMySellList(userName){
    return request({
        url: '/goods/getMySellGoods/' +　userName,
        method: 'get'
    })
}

export function search(keyword){
    return request({
        url: '/goods/search/' + keyword,
        method: 'get'
    })
}


export function del(del){

}
