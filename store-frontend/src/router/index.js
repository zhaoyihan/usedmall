import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from "@/views/user/Login.vue";
import Layout from "@/views/Layout.vue";
import UserInfo from "@/views/user/UserInfo.vue";
import Home from "@/views/Home.vue";
import ShopInfo from "@/components/ShopInfo.vue";
import BuyShop from "@/components/BuyShop.vue";
import GoodsCar from "@/components/GoodsCar.vue";
import UploadGoods from "@/components/UploadGoods.vue";
import SearchList from "@/components/SearchList.vue";
import Admin from "@/views/admin/Admin.vue";
import DateWatch from "@/components/admin/DateWatch.vue";
import GoodsManage from "@/components/admin/GoodsManage.vue";
import userInfoMan from "@/components/admin/UserInfoMan.vue";
import Chat from "@/components/Chat.vue";
import uploadNum from "@/components/admin/UploadNum.vue";
import OrderList from "@/components/OrderList.vue";

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Layout',
    component: Layout,
    children: [
      {
        path: '/',
        component: Home
      },
      {
        path: '/userinfo',
        component: UserInfo
      },
      {
        path: '/info',
        component: ShopInfo
      },
      {
        path: '/buy',
        component: BuyShop
      },
      {
        path: '/cart',
        component: GoodsCar
      },
      {
        path: '/upload',
        component: UploadGoods
      },
      {
        path: '/search',
        component: SearchList
      },
      {
        path: '/chat',
        component: Chat
      },
      {
        path: '/orderlist',
        component: OrderList
      }
    ]
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/admin',
    name: 'admin',
    component: Admin,
    children: [
      {
        path: '/watch',
        component: DateWatch
      },
      {
        path: '/goods',
        component: GoodsManage
      },
      {
        path: '/userman',
        component: userInfoMan
      },
      {
        path: '/num',
        component: uploadNum
      }
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
