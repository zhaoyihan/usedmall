// 鉴权工具类
<!-- YiHan -->

import store from './store/store'
import { Message } from 'element-ui'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
import { getToken, setToken } from '@/utils/auth'
import VueRouter from "vue-router";

Vue.config.productionTip = false

Vue.use(VueRouter)

const router = new VueRouter({
    // 在这里配置你的路由
    routes: [
        '/cart'
    ]
})

router.beforeEach((to, from, next) => {
    console.log(1)
    if(to.query.token) {
        setToken(to.query.token)
        store.commit('SET_TOKEN', to.query.token)
    }
    if (getToken()) {
        /* has token*/
        if (to.path === '/index') {
            next({ path: '/login' })
        } else {
            if (store.getters.roles.length === 0) {
                // 判断当前用户是否已拉取完user_info信息
                store.dispatch('GetInfo').then(res => {
                    // 拉取user_info
                    const roles = res.roles
                    store.dispatch('GenerateRoutes', { roles }).then(accessRoutes => {
                        // 测试 默认静态页面
                        // store.dispatch('permission/generateRoutes', { roles }).then(accessRoutes => {
                        // 根据roles权限生成可访问的路由表
                        router.addRoutes(accessRoutes) // 动态添加可访问路由表
                        next({ ...to, replace: true }) // hack方法 确保addRoutes已完成
                    })
                })
                    .catch(err => {
                        store.dispatch('FedLogOut').then(() => {
                            Message.error(err)
                            next({ path: '/' })
                        })
                    })
            } else {
                next()
            }
        }
    }
})

router.afterEach(() => {
    NProgress.done()
})
