// vue自带配置类
// YiHan

import Vue from 'vue'
import router from './router'
import store from './store/store'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import App from './App.vue'
import Router from "./router";
import axios from "axios";
Vue.prototype.$axios = axios;

import * as echarts from 'echarts'
Vue.prototype.$echarts = echarts

Vue.use(ElementUI, Router);

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
