package com.shop.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shop.domain.entity.UserOrder;

/**
 * 用户工单实体
 * @author YiHan
 **/
public interface UserOrderMapper extends BaseMapper<UserOrder> {
}