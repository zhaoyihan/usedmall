package com.shop.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shop.domain.entity.UserMsgContent;

import java.util.List;
import java.util.Map;

/**
*  @desc: 用户聊天信息
 * @author YiHan
 **/
public interface UserMsgContentMapper extends BaseMapper<UserMsgContent> {
    List<Map<String, Object>> selectMsg(UserMsgContent userMsgContent);
}