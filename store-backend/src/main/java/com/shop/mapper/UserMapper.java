package com.shop.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shop.domain.entity.User;

/**
 * 操作用户
 * @author YiHan
 **/

public interface UserMapper extends BaseMapper<User> {

}




