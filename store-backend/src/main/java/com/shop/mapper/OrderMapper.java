package com.shop.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shop.domain.entity.UserOrder;
import com.shop.domain.vo.UserOrderVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 操作订单
 * @author YiHan
 **/
public interface OrderMapper extends BaseMapper<UserOrder> {
    List<Map<String, String>> selectOrderList(@Param("userAccount") String userAccount, @Param("type") String type);
}
