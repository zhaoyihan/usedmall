package com.shop.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shop.domain.entity.Goods;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 操作商品
 * @author YiHan
 **/
public interface GoodsMapper extends BaseMapper<Goods> {
    List<Goods> selectGoosList();

    Goods selectGoodsInfo(String goodsId);

    List<Goods> selectRecommendList();

    List<Goods> searchList(@Param("keyword") String keyword);

    List<Map<String, String>> selectGoodsDate();

    List<Goods> selectGoodsList(Goods goods);

    List<Map<String, String>> selectUploadNum();

}