package com.shop.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shop.domain.entity.GoodsCar;

import java.util.List;
import java.util.Map;

/**
 * 操作购物车
 * @author YiHan
 **/
public interface GoodsCarMapper extends BaseMapper<GoodsCar> {


    List<Map<String, String>> selectGoodsCarList(String userId);
}