package com.shop.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shop.domain.entity.GoodsContent;

/**
 * 操作商品评论信息
 * @author YiHan
 **/

public interface GoodsContentMapper extends BaseMapper<GoodsContent> {
}