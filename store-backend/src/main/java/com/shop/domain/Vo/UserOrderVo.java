package com.shop.domain.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.shop.domain.entity.Goods;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;


/**
 * 用户订单vo实体
 * @author YiHan
 **/
@Data
@EqualsAndHashCode
public class UserOrderVo implements Serializable {

    /**
     * 商品编号
     */
    private String goodsId;

    /**
     * 工单编号
     */
    private String orderId;

    /**
     * 商品名称
     */
    private String title;

    /**
     * 图片
     */
    private String p1;

    /**
     * 卖家id
     */
    private String sellUser;

    /**
     * 买家id
     */
    private String buyerUser;

    /**
     * 商品价格
     */
    private Double price;

    /**
     * 邮费
     */
    private Double freight;

    /**
     * 实付款
     */
    private Double realPrice;

    /**
     * 下单时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date orderTime;

    /**
     * 付款时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date sellTime;

    /**
     * 发货时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date sendTime;

    /**
     * 完成时间(工单完结时间)
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    /**
     * 工单状态
     */
    private String status;

    private static final long serialVersionUID = 1L;
}
