package com.shop.domain.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 获取脱敏数据
 * 登录用户vo实体
 * @author YiHan
 **/
@Data
public class LoginUserVO implements Serializable {

    private String userId;

    /**
     * 账户
     */
    private String userAccount;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 头像
     */
    private String userAvatar;

    /**
     * 简介
     */
    private String desc;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 性别
     */
    private String sex;

    /**
     * 生日
     */
    private Date birthday;

    /**
     * 我的喜欢
     */
    private String bobby;

    /**
     * 角色
     */
    private String role;

    /**
     * 常住地
     */
    private String locate;

    /**
     * 创建时间
     */
    private Date createTime;

    private String token;

}