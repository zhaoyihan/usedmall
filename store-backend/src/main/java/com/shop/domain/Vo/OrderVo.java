package com.shop.domain.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.List;


/**
 * 购物车vo实体 进行结账
 * @author YiHan
 **/

@Data
@EqualsAndHashCode
public class OrderVo {

    private String userId;

    private String userAccount;

    private String shippingAddress;

    private Double emoPrice;

    private List<CartInfo> selectList;
}
