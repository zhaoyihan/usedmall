package com.shop.domain.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.shop.domain.entity.GoodsContent;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 商品vo实体
 * @author YiHan
 */

@Data
@EqualsAndHashCode
public class GoodsVo implements Serializable {

    private static final long serialVersionUID = 1L;


    private String goodsId;

    /**
     * 上架人
     */
    private String userName;

    /**
     * 商品标题
     */
    private String title;

    /**
     * 商品描述
     */
    private String description;

    /**
     * 类型id
     */
    private String category;

    /**
     * 品牌id
     */
    private String brand;

    /**
     * 新旧程度
     */
    private Double level;

    /**
     * 商品价格
     */
    private Double price;

    /**
     * 图片1
     */
    private String p1;

    /**
     * 图片2
     */
    @TableField(value = "p2")
    private String p2;

    /**
     * 图片3
     */
    @TableField(value = "p3")
    private String p3;

    /**
     * 图片4
     */
    private String p4;

    /**
     * 所在定位
     */
    private String location;

    /**
     * 发货方式
     */
    private String sendMethod;

    /**
     * 上架时间
     */
    private Date insertTime;

    /**
     * 评论列表
     */
    private List<GoodsContent> contendList;

    /**
     *  是否删除
     */
    private String isDelete;
}
