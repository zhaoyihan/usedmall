package com.shop.domain.vo;


import lombok.Data;

/**
 * 用于验证码校验登录
 * @author YiHan
 **/
@Data
public class UserVo {

    private String phone;

    private String code;
}
