package com.shop.domain.vo;


import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 购物车vo实体
 * @author YiHan
 */
@Data
@EqualsAndHashCode
public class CartInfo implements Serializable {

    /**
     * 商品编号
     */
    private String goodsId;

    /**
     * 上架人名称
     */
    private String userName;

    /**
     * 商品数量
     */
    private Integer count;

    /**
     * 总价格
     */
    private Double price;
}
