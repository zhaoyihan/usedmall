package com.shop.domain.entity;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 商品
 * @author YiHan
 */
@Data
@EqualsAndHashCode
@TableName(value = "goods")
@ExcelIgnoreUnannotated
public class Goods implements Serializable {

    private static final long serialVersionUID = 1L;


    @TableId(value = "goodsId", type = IdType.AUTO)
    private String goodsId;

    /**
     * 上架人
     */
    @TableField(value = "userName")
    private String userName;

    /**
     * 商品标题
     */
    @TableField(value = "title")
    @ExcelProperty(value = "title")
    private String title;

    /**
     * 商品描述
     */
    @TableField(value = "description")
    @ExcelProperty(value = "description")
    private String description;

    /**
     * 类型id
     */
    @TableField(value = "category")
    @ExcelProperty(value = "category")
    private String category;

    /**
     * 品牌id
     */
    @TableField(value = "brand")
    @ExcelProperty(value = "brand")
    private String brand;

    /**
     * 新旧程度
     */
    @TableField(value = "`level`")
    @ExcelProperty(value = "level")
    private Double level;

    /**
     * 商品价格
     */
    @TableField(value = "price")
    @ExcelProperty(value = "price")
    private Double price;

    /**
     * 图片1
     */
    @TableField(value = "p1")
    private String p1;

    /**
     * 图片2
     */
    @TableField(value = "p2")
    private String p2;

    /**
     * 图片3
     */
    @TableField(value = "p3")
    private String p3;

    /**
     * 图片4
     */
    @TableField(value = "p4")
    private String p4;

    /**
     * 所在定位
     */
    @TableField(value = "`location`")
    @ExcelProperty(value = "location")
    private String location;

    /**
     * 发货方式
     */
    @TableField(value = "sendMethod")
    @ExcelProperty(value = "sendMethod")
    private String sendMethod;

    /**
     * 上架时间
     */
    @TableField(value = "insertTime")
    @ExcelProperty(value = "insertTime")
    private Date insertTime;

    /**
     *  是否删除
     */
    @TableField(value = "isDelete")
    private String isDelete;
}