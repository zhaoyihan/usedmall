package com.shop.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * @desc: 用户订单
 * @author YiHan
 */
@Data
@TableName(value = "user_order")
public class UserOrder implements Serializable {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 商品编号
     */
    @TableField(value = "goodsId")
    private String goodsId;

    /**
     * 工单编号
     */
    @TableField(value = "orderId")
    private String orderId;

    /**
     * 卖家id
     */
    @TableField(value = "sellUser")
    private String sellUser;

    /**
     * 买家id
     */
    @TableField(value = "buyerUser")
    private String buyerUser;

    /**
     * 数量
     */
    @TableField(value = "num")
    private Integer num;

    /**
     * 商品价格
     */
    @TableField(value = "price")
    private Double price;

    /**
     * 邮费
     */
    @TableField(value = "freight")
    private Double freight;

    /**
     * 实付款
     */
    @TableField(value = "realPrice")
    private Double realPrice;

    /**
     * 收货地址
     */
    @TableField(value = "address")
    private String address;

    /**
     * 下单时间
     */
    @TableField(value = "orderTime")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date orderTime;

    /**
     * 付款时间
     */
    @TableField(value = "sellTime")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date sellTime;

    /**
     * 发货时间
     */
    @TableField(value = "sendTime")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date sendTime;

    /**
     * 完成时间(工单完结时间)
     */
    @TableField(value = "endTime")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    /**
     * 工单状态
     */
    @TableField(value = "`status`")
    private String status;

    /**
     * 是否被删除
     */
    @TableField(value = "isDelete")
    private String isDelete;

    private static final long serialVersionUID = 1L;
}