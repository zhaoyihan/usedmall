package com.shop.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户聊天信息
 * @author YiHan
 */
@Data
@TableName(value = "user_msg_content")
public class UserMsgContent implements Serializable {
    /**
     * Id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 发送人
     */
    @TableField(value = "sendUser")
    private String sendUser;

    /**
     * 发送内容
     */
    @TableField(value = "sendContent")
    private String sendContent;

    /**
     * 接收人
     */
    @TableField(value = "receiveUser")
    private String receiveUser;

    /**
     * 发送时间
     */
    @TableField(value = "sendTime")
    private Date sendTime;

    /**
     * 是否已读
     */
    @TableField(value = "isRead")
    private String idRead;

    private static final long serialVersionUID = 1L;
}