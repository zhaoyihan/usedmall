package com.shop.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 评论类
 * @author YiHan
 */
@Data
@TableName(value = "goods_content")
@EqualsAndHashCode
@ToString
public class GoodsContent implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private String id;

    /**
     * 商品id
     */
    @TableField(value = "goodsId")
    private Long goodsId;

    /**
     * 用户id
     */
    @TableField(value = "userId")
    private Long userId;

    /**
     * 用户昵称
     */
    @TableField(value = "userName")
    private String userName;

    /**
     * 得分
     */
    @TableField(value = "scope")
    private Integer scope;

    /**
     * 评论
     */
    @TableField(value = "content")
    private String content;

    /**
     * 评论时间
     */
    @TableField(value = "contentTime")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date contentTime;

}