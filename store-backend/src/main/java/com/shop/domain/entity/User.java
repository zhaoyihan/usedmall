package com.shop.domain.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;

/**
 * 用户
 * @author YiHan
 */
@Data
@EqualsAndHashCode
@TableName(value = "user_info")
public class User implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "userId", type = IdType.AUTO)
    private String userId;

    /**
     * 账户
     */
    @TableField("userAccount")
    private String userAccount;

    /**
     * 密码
     */
    @TableField("userPassword")
    private String userPassword;

    /**
     * 用户名
     */
    @TableField("userName")
    private String userName;

    /**
     * 头像
     */
    @TableField("userAvatar")
    private String userAvatar;

    /**
     * 简介
     */
    @TableField("`desc`")
    private String desc;

    /**
     * 手机号
     */
    @TableField("phone")
    private String phone;

    /**
     * 性别
     */
    @TableField("sex")
    private String sex;

    /**
     * 生日
     */
    @TableField("birthday")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date birthday;

    /**
     * 我的喜欢
     */
    @TableField("bobby")
    private String bobby;

    /**
     * 角色
     */
    @TableField("role")
    private String role;

    /**
     * 常住地
     */
    @TableField("locate")
    private String locate;

    /**
     * 创建时间
     */
    @TableField("createTime")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 是否删除
     */
    @TableField("isDelete")
    private String isDelete;

    /**
     * token
     */
    @TableField("token")
    private String token;

    /**
     * 验证码
     */
    @TableField("phoneCode")
    private String phoneCode;

}