package com.shop.controller;


import cn.hutool.db.sql.Order;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.shop.common.AjaxResult;
import com.shop.domain.entity.UserOrder;
import com.shop.domain.vo.OrderVo;
import com.shop.service.OrderService;
import org.aspectj.weaver.ast.Or;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 订单模块
 * @author YiHan
 */
@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    /**
     * 创建订单
     * @param orderVo
     * @return
     */
    @PostMapping("/add")
    public AjaxResult addOrder(@RequestBody OrderVo orderVo){
        return orderService.addOrder(orderVo);
    }

    @GetMapping("/getOrderList/{userAccount}/{type}")
    public AjaxResult getOrderList(@PathVariable String userAccount, @PathVariable String type){
        return AjaxResult.success(orderService.getOrderList(userAccount, type));
    }

    @GetMapping("/del/{id}")
    public AjaxResult delOrder(@PathVariable String id){
        QueryWrapper<UserOrder> userOrderQueryWrapper = new QueryWrapper<>();
        userOrderQueryWrapper.eq("id", id);
        if (orderService.remove(userOrderQueryWrapper)){
            return AjaxResult.success("删除成功");
        } else {
            return AjaxResult.error("删除失败");
        }
    }
}
