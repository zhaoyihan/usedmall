package com.shop.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.shop.common.AjaxResult;
import com.shop.domain.entity.GoodsCar;
import com.shop.service.GoodsCarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 用户购物车
 * @author YiHan
 */
@RestController
@RequestMapping("/goodsCar")
public class GoodsCardController {

    @Autowired
    private GoodsCarService goodsCarService;

    /**
     * 根据用户id查询用户购物车
     * @param userId
     * @return
     */
    @GetMapping("/getGoodsList/{userId}")
    public AjaxResult getGoodsList(@PathVariable String userId){
        return AjaxResult.success(goodsCarService.getGoodsCarList(userId));
    }
    /**
     * * 添加购物车
     * @param goodsCar
     * @return
     */
    @PostMapping("/add")
    public AjaxResult addGoodCar(@RequestBody GoodsCar goodsCar){
        if (goodsCarService.save(goodsCar)){
            return AjaxResult.success("Added successfully");
        } else {
            return AjaxResult.error("add failed");
        }
    }

    /**
     * 删除购物车
     * @param goodsCar
     * @return
     */
    @PostMapping("/deleteFromCart")
    public AjaxResult delGoodCar(@RequestBody GoodsCar goodsCar){
        QueryWrapper<GoodsCar> goodsCarQueryWrapper = new QueryWrapper<>();
        goodsCarQueryWrapper.eq("userId", goodsCar.getUserId());
        goodsCarQueryWrapper.eq("goodsId", goodsCar.getGoodsId());
        if (goodsCarService.remove(goodsCarQueryWrapper)){
            return AjaxResult.success("successfully deleted");
        } else {
            return AjaxResult.error("failed to delete");
        }
    }
}
