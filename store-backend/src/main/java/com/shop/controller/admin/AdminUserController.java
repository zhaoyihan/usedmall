package com.shop.controller.admin;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.shop.common.AjaxResult;
import com.shop.domain.entity.User;
import com.shop.service.UserService;
import com.shop.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

/**
 * 管理员用户信息
 * @author YiHan
 */
@RestController
@RequestMapping("/admin/userinfo")
public class AdminUserController {


    @Autowired
    private UserService userService;

    @GetMapping("/list")
    public AjaxResult getUserList(User user) {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(user.getUserAccount())) {
            queryWrapper.eq("userAccount", user.getUserAccount());
            return AjaxResult.success(userService.getOne(queryWrapper));
        }
        if (StringUtils.isNotEmpty(user.getUserName())) {
            queryWrapper.eq("userName", user.getUserName());
            return AjaxResult.success(userService.getOne(queryWrapper));
        }
        return AjaxResult.success(userService.list());
    }

    @GetMapping("/del/{userId}")
    public AjaxResult delUser(@PathVariable String userId){
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("userId", userId);
        userService.remove(queryWrapper);
        return AjaxResult.success();
    }
}
