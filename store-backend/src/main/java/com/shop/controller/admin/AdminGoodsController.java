package com.shop.controller.admin;


import com.shop.common.AjaxResult;
import com.shop.domain.entity.Goods;
import com.shop.domain.vo.GoodsVo;
import com.shop.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 管理员接口
 * @author YiHan
 */
@RestController
@RequestMapping("/admin/goods")
public class AdminGoodsController {

    @Autowired
    private GoodsService goodsService;

    @GetMapping("/getDate")
    public AjaxResult getGoodsDate() {
        return AjaxResult.success(goodsService.getGoodsDate());
    }

    @GetMapping("/getUploadNum")
    public AjaxResult getUploadNum() {
        return AjaxResult.success(goodsService.getUploadNum());
    }

    @GetMapping("/list")
    public AjaxResult orderList() {
        return AjaxResult.success(goodsService.list());
    }

    @GetMapping("/getList")
    public AjaxResult getList(Goods goods) {
        return AjaxResult.success(goodsService.getList(goods));
    }

    @GetMapping("/down/{goodsId}")
    public AjaxResult down(@PathVariable String goodsId) {
        GoodsVo goodsvo = goodsService.getGoodsInfo(goodsId);
        Goods goods = new Goods();
        if ("0".equals(goodsvo.getIsDelete())){
            goods.setGoodsId(goodsId);
            goods.setIsDelete("1");
        } else {
            goods.setGoodsId(goodsId);
            goods.setIsDelete("0");
        }
        if (goodsService.updateById(goods)) {
            return AjaxResult.success("操作成功");
        } else {
            return AjaxResult.error();
        }
    }
}
