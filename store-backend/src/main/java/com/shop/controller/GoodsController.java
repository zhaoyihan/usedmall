package com.shop.controller;


import cn.hutool.core.lang.UUID;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.shop.common.AjaxResult;
import com.shop.domain.entity.Goods;
import com.shop.domain.entity.GoodsContent;
import com.shop.domain.vo.GoodsVo;
import com.shop.service.GoodsContentService;
import com.shop.service.GoodsService;
import com.shop.util.QiniuUtil;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.io.File;
import java.lang.reflect.Field;

/**
 * 商品
 * @author YiHan
 */
@RestController
@RequestMapping("/goods")
@MultipartConfig
public class GoodsController {


    @Autowired
    private GoodsService goodsService;

    @Autowired
    private GoodsContentService goodsContentService;

    /**
     * 查询轮播图
     * @return
     */
    @GetMapping("/getPhotoList")
    public AjaxResult getPhotoList(){
        return AjaxResult.success(goodsService.getGoosList());
    }

    /**
     * 查询商品信息
     * @param goodsId
     * @return
     */
    @GetMapping("/getGoodInfo/{goodsId}" )
    public AjaxResult getGoodInfo(@PathVariable String goodsId){
        return AjaxResult.success(goodsService.getGoodsInfo(goodsId));
    }

    /**
     * 添加评论
     */
    @PostMapping("/addGoodsContent")
    public AjaxResult addGoodsContent(@RequestBody GoodsContent goodsContent){
        return goodsContentService.saveContent(goodsContent);
    }

    @GetMapping("/getRecommendList")
    public AjaxResult getRecommendList(){
        return AjaxResult.success(goodsService.getRecommendList());
    }

    @GetMapping("/getMySellGoods/{userName}")
    public AjaxResult getMySellGoods(@PathVariable String userName){
        QueryWrapper<Goods> goodsQueryWrapper = new QueryWrapper<>();
        goodsQueryWrapper.eq("userName", userName);
        return AjaxResult.success(goodsService.list(goodsQueryWrapper));
    }

    @GetMapping("/search/{keyword}")
    public AjaxResult search(@PathVariable String keyword){
        return AjaxResult.success(goodsService.search(keyword));
    }

    @PostMapping("/add")
    public AjaxResult add(@RequestBody GoodsVo goodsVo){
        return goodsService.addGoods(goodsVo);
    }

    @GetMapping("/del/{goodsId}")
    public AjaxResult delGoods(@PathVariable String goodsId){
        QueryWrapper<Goods> goodsQueryWrapper = new QueryWrapper<>();
        goodsQueryWrapper.eq("goodsId", goodsId);
        if (goodsService.remove(goodsQueryWrapper)){
            return AjaxResult.success();
        } else {
            return  AjaxResult.error("操作失败");
        }
    }

    @PostMapping(value = "/upload")
    @ResponseBody
    public AjaxResult upload(@RequestParam MultipartFile file) {
        try {
            // 获取原始文件名
            String originalFilename = file.getOriginalFilename();
            int lastIndexOf = originalFilename.lastIndexOf(".");
            // 获取文件后缀
            String suffix = originalFilename.substring(lastIndexOf); // 修正后缀名获取方式

            // 使用UUID随机产生文件名称，防止同名文件覆盖
            String fileName = UUID.fastUUID() + suffix;

            byte[] fileBytes = file.getBytes();

            // 上传文件到七牛云并获取 URL
            String url = QiniuUtil.upload2Qiniu(fileBytes, fileName);

            // 返回成功响应和文件 URL
            return AjaxResult.success("Upload successful", url);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error("upload failed");
        }
    }
}
