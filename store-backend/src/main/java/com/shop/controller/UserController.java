package com.shop.controller;

import cn.hutool.core.math.MathUtil;
import cn.hutool.core.util.RandomUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.shop.common.AjaxResult;
import com.shop.domain.entity.User;
import com.shop.domain.entity.UserMsgContent;
import com.shop.domain.vo.UserVo;
import com.shop.service.MsgService;
import com.shop.service.UserService;
import com.shop.util.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import static cn.hutool.crypto.SecureUtil.md5;
import static com.shop.util.UserUtil.toLoginUserInfo;

/**
 * 用户控制层
 * @author YiHan
 */

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private MsgService msgService;

    /**
     * 用户登录
     */
    @PostMapping("/login/{loginType}")
    public AjaxResult login(@RequestBody User user, @PathVariable String loginType) {
        return userService.login(user, loginType);
    }

    /**
     * 查询用户信息
     *
     * @param userId
     * @return
     */
    @GetMapping("/getUserInfo/{userId}")
    public AjaxResult getUserInfo(@PathVariable Long userId) {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("userId", userId);
        User user = userService.getOne(queryWrapper);
        return AjaxResult.success(toLoginUserInfo(user));
    }

    /**
     * 生成验证码
     *
     * @param phone
     * @return
     */
    @GetMapping("/getPhoneCode/{phone}")
    public AjaxResult getPhoneCode(@PathVariable String phone) {
        String phoneCode = RandomUtil.randomNumbers(6);
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("phone", phone);
        User phoneUser = userService.getOne(queryWrapper);
        if (phoneUser != null) {
            queryWrapper.clear();
            queryWrapper.eq("phone", phone);
            User user = new User();
            user.setPhoneCode(phoneCode);
            userService.update(user, queryWrapper);
            return AjaxResult.success(phoneCode);
        } else {
            User user = new User();
            user.setPhone(phone);
            user.setPhoneCode(phoneCode);
            user.setCreateTime(DateUtils.getNowDate());
            userService.save(user);
            return AjaxResult.success(phoneCode);
        }
    }

    /**
     * 校验验证码
     *
     * @param userVo
     * @return
     */
    @PostMapping("/checkPhoneNum")
    public AjaxResult checkPhoneNum(@RequestBody UserVo userVo) {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("phone", userVo.getPhone());
        queryWrapper.eq("phoneCode", userVo.getCode());
        User user = userService.getOne(queryWrapper);
        if (user != null) {
            return AjaxResult.success(toLoginUserInfo(user));
        } else {
            return AjaxResult.error("Verification code is wrong, please re-enter");
        }
    }

    /**
     * 手机号确定校验之后 保存信息
     *
     * @param user
     * @return
     */
    @PostMapping("/updateUserInfo")
    public AjaxResult addInfo(@RequestBody User user) {
        user.setUserPassword(md5(user.getUserPassword()));
        if (userService.updateById(user)) {
            return AjaxResult.success(toLoginUserInfo(user));
        } else {
            return AjaxResult.error("操作失败,请稍后重试");
        }
    }

    @PostMapping("/saveUserInfo")
    public AjaxResult saveUserInfo(@RequestBody User user) {
        if (userService.updateById(user)) {
            return AjaxResult.success();
        } else {
            return AjaxResult.error();
        }
    }


    @PostMapping("/msg/send")
    public AjaxResult send(@RequestBody UserMsgContent userMsgContent){
        userMsgContent.setSendTime(DateUtils.getNowDate());
        userMsgContent.setIdRead("0");
        msgService.save(userMsgContent);
        return AjaxResult.success();
    }

    @GetMapping("/msg/getMsg")
    public AjaxResult getMsg(UserMsgContent userMsgContent){
        return AjaxResult.success(msgService.getMsg(userMsgContent));
    }
}
