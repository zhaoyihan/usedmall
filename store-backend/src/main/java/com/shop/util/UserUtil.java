package com.shop.util;

import com.shop.domain.entity.User;
import com.shop.domain.vo.LoginUserVO;
import org.springframework.beans.BeanUtils;


/**
 * 将获取到的信息 转换为脱敏的信息
 * @author YiHan
 **/
public class UserUtil {


    public static LoginUserVO toLoginUserInfo(User user){
        LoginUserVO loginUserVO = new LoginUserVO();
        BeanUtils.copyProperties(user, loginUserVO);
        return loginUserVO;
    }
}
