package com.shop.util;

import cn.hutool.jwt.JWT;
import com.shop.domain.entity.User;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;

/**
 * JWT 工具类
 * @author YiHan
 **/
public class JWTUtility {

    /**
     * 密钥
     */
    private static final byte[] KEY = "zibo".getBytes();

    /**
     * 过期时间（秒）：7 天
     */
    public static final long EXPIRE = 7 * 24 * 60 * 60;

    private JWTUtility() {
    }

    /**
     * 根据 userDto 生成 token
     *
     * @param user    用户信息
     * @return token
     */
    public static String generateTokenForUser(User user) {
        Map<String, Object> map = new HashMap<>();
        map.put("userId", user.getUserId());
        map.put("userName", user.getUserName());
        return generateToken(map);
    }

    /**
     * 根据 map 生成 token 默认：HS265(HmacSHA256)算法
     *
     * @param map    携带数据
     * @return token
     */
    public static String generateToken(Map<String, Object> map) {
        JWT jwt = JWT.create();
        // 设置携带数据
        map.forEach(jwt::setPayload);
        // 设置密钥
        jwt.setKey(KEY);
        // 设置过期时间
        jwt.setExpiresAt(new Date(System.currentTimeMillis() + EXPIRE * 1000));
        return jwt.sign();
    }

    /**
     * token 校验
     * @param token token
     * @return 是否通过校验
     */
    public static boolean verify (String token) {
        if (StringUtils.isBlank(token)) return false;
        return JWT.of(token).setKey(KEY).verify();
    }

}
