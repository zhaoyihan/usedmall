package com.shop.util;

import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.lang.UUID;
import cn.hutool.core.util.IdUtil;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * ID生成器工具类
 * @author YiHan
 **/
public class IdUtils {


    private static long tmpID = 0;

    private static final long LOCK_TIME = 1;

    private static final long INCREASE_STEP = 1;

    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");

    private static final Lock LOCK = new ReentrantLock();


    /**
     * 获取ID
     */
    private static long getID() throws InterruptedException {

        long timeCount;
        if (LOCK.tryLock(LOCK_TIME, TimeUnit.SECONDS)) {
            timeCount = Long.parseLong(sdf.format(new Date()));
            try {
                if (tmpID < timeCount) {
                    tmpID = timeCount;
                } else {
                    tmpID += INCREASE_STEP;
                    timeCount = tmpID;
                }
                return timeCount;
            } finally {
                LOCK.unlock();
            }
        } else {
            return getID();
        }
    }

    /**
     * 生成工单编号.以时间戳的方式
     */
    public static String getOrderID() {
        String orderID = "";
        try {
            orderID = String.valueOf(getID());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return orderID;
    }
}
