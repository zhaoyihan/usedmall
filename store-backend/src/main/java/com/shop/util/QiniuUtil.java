package com.shop.util;

import com.google.gson.Gson;
import com.qiniu.common.QiniuException;
import com.qiniu.http.Response;
import com.qiniu.storage.BucketManager;
import com.qiniu.storage.Region;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.util.Auth;
import com.qiniu.storage.Configuration;

import java.io.IOException;


/**
 * 七牛云上传工具类
 * @author YiHan
 **/
public class QiniuUtil {
    //设置需要操作的账号的AK和SK
    private static final String ACCESS_KEY = "z7tYq0VxI-wGZPhUyxd7l04eYzO_c1VB-BpBa-yx";
    private static final String SECRET_KEY = "u2qWDll7m0rCcUm5-qiadLmTAiwc6xmccxXMDxnM";

    //要上传的空间
    private static final String bucketname = "thrifthaven";

    private static final String domainOfBucket = "s4vwu53uh.bkt.gdipper.com";

    //密钥
    private static final Auth auth = Auth.create(ACCESS_KEY, SECRET_KEY);

    //上传文件
    public static void upload2Qiniu(String filePath,String fileName){
        //构造一个带指定Zone对象的配置类
        Configuration cfg = new Configuration(Region.huadong());
        UploadManager uploadManager = new UploadManager(cfg);
        Auth auth = Auth.create(ACCESS_KEY, SECRET_KEY);
        String upToken = auth.uploadToken(bucketname);
        try {
            Response response = uploadManager.put(filePath, fileName, upToken);
            //解析上传成功的结果
            DefaultPutRet putRet =
                    new Gson().fromJson(response.bodyString(), DefaultPutRet.class);
        } catch (QiniuException ex) {
            Response r = ex.response;
            try {
                System.err.println(r.bodyString());
            } catch (QiniuException ex2) {
                //ignore
            }
        }
    }

    public static String getUploadedFileUrl(String fileName) {
        // 获取上传文件的 key
        String key = fileName;

        // 存储空间域名，例如："http://example.com/"
        String domainOfBucket = "http://s4vwu53uh.bkt.gdipper.com";

        // 生成公开访问的 URL
        String publicFileUrl = domainOfBucket + "/" + key;

        return publicFileUrl;
    }

    //上传文件
    public static String upload2Qiniu(byte[] bytes, String fileName){
        //构造一个带指定Zone对象的配置类
        Configuration cfg = new Configuration(Region.beimei());
        //...其他参数参考类注释
        UploadManager uploadManager = new UploadManager(cfg);
        //默认不指定key的情况下，以文件内容的hash值作为文件名
        String key = fileName;
        Auth auth = Auth.create(ACCESS_KEY, SECRET_KEY);
        String upToken = auth.uploadToken(bucketname);
        try {
            Response response = uploadManager.put(bytes, key, upToken);
            //解析上传成功的结果
            DefaultPutRet putRet =
                    new Gson().fromJson(response.bodyString(), DefaultPutRet.class);
            // 获取并打印文件访问 URL
            String fileUrl = getUploadedFileUrl(putRet.key);
            return fileUrl;
        } catch (QiniuException ex) {
            Response r = ex.response;
            System.err.println(r.toString());
            try {
                System.err.println(r.bodyString());
            } catch (QiniuException ex2) {
                //ignore
            }
            return r.toString();
        }
    }

    //删除文件
    public static void deleteFileFromQiniu(String fileName){
        //构造一个带指定Zone对象的配置类
        Configuration cfg = new Configuration(Region.huadong());
        String key = fileName;
        Auth auth = Auth.create(ACCESS_KEY, SECRET_KEY);
        BucketManager bucketManager = new BucketManager(auth, cfg);
        try {
            bucketManager.delete(bucketname, key);
        } catch (QiniuException ex) {
            //如果遇到异常，说明删除失败
            System.err.println(ex.code());
            System.err.println(ex.response.toString());
        }
    }
}
