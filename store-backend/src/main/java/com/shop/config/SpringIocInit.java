package com.shop.config;

import jakarta.servlet.MultipartConfigElement;
import jakarta.servlet.ServletRegistration;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/**
 * 配置
 * @author YiHan
 */
public class SpringIocInit extends AbstractAnnotationConfigDispatcherServletInitializer {

    /**
     * 基本配置
     * @return
     */
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{DataSourceJavaConfig.class, MyBatisPlusConfig.class, ServiceConfig.class};
    }

    /**
     * web容器的配置
     * @return
     */
    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{WebMvcConfig.class, MultipartConfig.class};
    }

    /**
     * dispatchservlet的拦截路径
     * @return
     */
    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }


    /**
     * 文件上传
     * @param registration
     */
    @Override
    protected void customizeRegistration(ServletRegistration.Dynamic registration) {
        // 配置多部分上传的参数
        MultipartConfigElement multipartConfigElement = new MultipartConfigElement("E:/project/store/store-backend/src/main/tmp");
        registration.setMultipartConfig(multipartConfigElement);

    }
}
