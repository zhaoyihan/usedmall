package com.shop.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.shop.common.AjaxResult;
import com.shop.domain.entity.User;

/**
 * 用户业务
 * @author YiHan
 **/
public interface UserService extends IService<User> {

    AjaxResult login(User user, String loginType);
}
