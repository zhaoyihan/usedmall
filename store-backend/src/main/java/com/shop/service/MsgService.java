package com.shop.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.shop.domain.entity.UserMsgContent;

import java.util.List;
import java.util.Map;

/**
 * 消息业务
 * @author YiHan
 **/
public interface MsgService extends IService<UserMsgContent> {
    List<Map<String, Object>> getMsg(UserMsgContent userMsgContent);
}
