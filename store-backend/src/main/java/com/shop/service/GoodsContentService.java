package com.shop.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.shop.common.AjaxResult;
import com.shop.domain.entity.GoodsContent;


/**
 * 评论业务
 * @author YiHan
 **/
public interface GoodsContentService extends IService<GoodsContent> {
    AjaxResult saveContent(GoodsContent goodsContent);
}
