package com.shop.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.shop.common.AjaxResult;
import com.shop.domain.entity.Goods;
import com.shop.domain.vo.GoodsVo;

import java.util.List;
import java.util.Map;

/**
 * 商品业务
 * @author YiHan
 **/
public interface GoodsService extends IService<Goods> {
    List<Goods> getGoosList();

    GoodsVo getGoodsInfo(String goodsId);

    List<Goods> getRecommendList();

    List<Goods> search(String keyword);

    AjaxResult addGoods(GoodsVo goodsVo);

    List<Map<String, String>> getGoodsDate();

    List<Goods> getList(Goods goods);

    List<Map<String, String>> getUploadNum();

}
