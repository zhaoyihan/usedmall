package com.shop.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.shop.common.AjaxResult;
import com.shop.domain.entity.UserOrder;
import com.shop.domain.vo.OrderVo;
import com.shop.domain.vo.UserOrderVo;

import java.util.List;
import java.util.Map;

/**
 * 订单业务
 * @author YiHan
 **/
public interface OrderService extends IService<UserOrder> {
    AjaxResult addOrder(OrderVo orderVo);

    List<Map<String, String>> getOrderList(String userAccount, String type);

}
