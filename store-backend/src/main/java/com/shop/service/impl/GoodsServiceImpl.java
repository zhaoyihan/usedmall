package com.shop.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.shop.common.AjaxResult;
import com.shop.domain.entity.Goods;
import com.shop.domain.entity.GoodsContent;
import com.shop.domain.vo.GoodsVo;
import com.shop.mapper.GoodsContentMapper;
import com.shop.mapper.GoodsMapper;
import com.shop.mapper.UserMapper;
import com.shop.service.GoodsService;
import jakarta.enterprise.inject.spi.Bean;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


/**
 * 商品业务
 * @author YiHan
 **/
@Service
public class GoodsServiceImpl extends ServiceImpl<GoodsMapper, Goods> implements GoodsService {


    @Autowired
    private GoodsMapper goodsMapper;

    @Autowired
    private GoodsContentMapper goodsContentMapper;

    @Autowired
    private UserMapper userMapper;

    @Override
    public List<Goods> getGoosList() {
        return goodsMapper.selectGoosList();
    }

    /**
     * 查询商品详情
     * @param goodsId
     * @return
     */
    @Override
    public GoodsVo getGoodsInfo(String goodsId) {
        Goods goods = goodsMapper.selectGoodsInfo(goodsId);
        GoodsVo goodsVo = new GoodsVo();
        goods.setLevel(goods.getLevel() * 10);
        QueryWrapper<GoodsContent> goodsContentQueryWrapper = new QueryWrapper<>();
        goodsContentQueryWrapper.eq("goodsId", goodsId);
        goodsContentQueryWrapper.orderByAsc("contentTime");
        // 查询评论
        List<GoodsContent> goodsContents = goodsContentMapper.selectList(goodsContentQueryWrapper);
        BeanUtils.copyProperties(goods, goodsVo);
        goodsVo.setContendList(goodsContents);
        return goodsVo;
    }

    @Override
    public List<Goods> getRecommendList() {
        return goodsMapper.selectRecommendList();
    }

    @Override
    public List<Goods> search(String keyword) {
        return goodsMapper.searchList(keyword);
    }

    @Override
    public AjaxResult addGoods(GoodsVo goodsVo) {
        Goods goods = new Goods();
        BeanUtils.copyProperties(goodsVo, goods);
        if (goodsMapper.insert(goods) >0){
            return AjaxResult.success("添加成功");
        } else {
            return AjaxResult.error("添加失败");
        }
    }

    @Override
    public List<Map<String, String>> getGoodsDate() {
        return goodsMapper.selectGoodsDate();
    }

    @Override
    public List<Goods> getList(Goods goods) {
        return goodsMapper.selectGoodsList(goods);
    }

    @Override
    public List<Map<String, String>> getUploadNum() {
        return goodsMapper.selectUploadNum();
    }
}
