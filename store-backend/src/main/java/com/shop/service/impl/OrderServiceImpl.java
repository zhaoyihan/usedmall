package com.shop.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.shop.common.AjaxResult;
import com.shop.domain.entity.Goods;
import com.shop.domain.entity.GoodsCar;
import com.shop.domain.entity.User;
import com.shop.domain.entity.UserOrder;
import com.shop.domain.vo.CartInfo;
import com.shop.domain.vo.OrderVo;
import com.shop.domain.vo.UserOrderVo;
import com.shop.mapper.GoodsCarMapper;
import com.shop.mapper.GoodsMapper;
import com.shop.mapper.OrderMapper;
import com.shop.service.GoodsCarService;
import com.shop.service.OrderService;
import com.shop.util.DateUtils;
import com.shop.util.IdUtils;
import org.aspectj.weaver.loadtime.Aj;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 订单业务
 * @author YiHan
 **/
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, UserOrder> implements OrderService {

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private GoodsMapper goodsMapper;

    @Autowired
    private GoodsCarMapper goodsCarMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public AjaxResult addOrder(OrderVo orderVo) {
        try {
            List<CartInfo> selectList = orderVo.getSelectList();
            String orderId = IdUtils.getOrderID();
            for (CartInfo cartInfo : selectList) {
                UserOrder userOrder = new UserOrder();
                // 工单号
                String goodsId = cartInfo.getGoodsId();
                String shippingAddress = orderVo.getShippingAddress();
                String sellUser = cartInfo.getUserName();
                String buyerUser = orderVo.getUserAccount();
                Double price = goodsMapper.selectGoodsInfo(goodsId).getPrice();
                // 数量
                Integer count = cartInfo.getCount();
                // 实付款
                Double realPrice = cartInfo.getPrice() * cartInfo.getCount();
                // 下单时间
                Date orderTime = DateUtils.getNowDate();
                // 支付时间
                Date sellTime = DateUtils.getNowDate();

                userOrder.setOrderId(orderId);
                userOrder.setGoodsId(goodsId);
                userOrder.setSellUser(sellUser);
                userOrder.setBuyerUser(buyerUser);
                userOrder.setAddress(shippingAddress);
                userOrder.setPrice(price);
                userOrder.setFreight(orderVo.getEmoPrice());
                userOrder.setRealPrice(realPrice);
                userOrder.setOrderTime(orderTime);
                userOrder.setSellTime(sellTime);
                userOrder.setNum(count);
                // 000 流程中
                userOrder.setStatus("000");

                orderMapper.insert(userOrder);
            }
            // 结算成功之后 从购物车删除
            List<String> goodsIds = orderVo.getSelectList().stream().map(CartInfo::getGoodsId).collect(Collectors.toList());
            for (String goodsId : goodsIds) {
                QueryWrapper<GoodsCar> goodsCarQueryWrapper = new QueryWrapper<>();
                goodsCarQueryWrapper.eq("userId", orderVo.getUserId());
                goodsCarQueryWrapper.eq("goodsId", goodsId);
                goodsCarMapper.delete(goodsCarQueryWrapper);
            }
            return AjaxResult.success(orderId);
        } catch (Exception e) {
            return AjaxResult.error("订单创建失败");
        }
    }


    @Override
    public List<Map<String,String>> getOrderList(String userAccount, String type) {
        return orderMapper.selectOrderList(userAccount, type);
    }
}
