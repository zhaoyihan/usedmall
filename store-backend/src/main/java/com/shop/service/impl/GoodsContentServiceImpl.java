package com.shop.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.shop.common.AjaxResult;
import com.shop.domain.entity.GoodsContent;
import com.shop.domain.entity.UserOrder;
import com.shop.mapper.GoodsContentMapper;
import com.shop.mapper.UserOrderMapper;
import com.shop.service.GoodsContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * 评论业务
 * @author YiHan
 **/
@Service
public class
GoodsContentServiceImpl extends ServiceImpl<GoodsContentMapper, GoodsContent> implements GoodsContentService {

    @Autowired
    private UserOrderMapper userOrderMapper;

    @Autowired
    private GoodsContentMapper goodsContentMapper;

    @Override
    public AjaxResult saveContent(GoodsContent goodsContent) {
        // 如果没有买过商品 则不能进行评论
        QueryWrapper<UserOrder> userOrderQueryWrapper = new QueryWrapper<>();
        userOrderQueryWrapper.eq("buyerUser", goodsContent.getUserName());
        userOrderQueryWrapper.eq("goodsId", goodsContent.getGoodsId());
        List<UserOrder> userOrders = userOrderMapper.selectList(userOrderQueryWrapper);
        if (CollectionUtil.isNotEmpty(userOrders)){
            return AjaxResult.toAjax(goodsContentMapper.insert(goodsContent));
        } else {
            return AjaxResult.error("您没有购买过此物品,暂时不能评论");
        }
    }
}
