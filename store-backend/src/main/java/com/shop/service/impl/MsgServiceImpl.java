package com.shop.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.shop.domain.entity.UserMsgContent;
import com.shop.mapper.UserMsgContentMapper;
import com.shop.service.MsgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 消息业务
 * @author YiHan
 **/
@Service
public class MsgServiceImpl extends ServiceImpl<UserMsgContentMapper, UserMsgContent> implements MsgService {

    @Autowired
    private UserMsgContentMapper userMsgContentMapper;

    @Override
    public List<Map<String, Object>> getMsg(UserMsgContent userMsgContent) {
        List<Map<String, Object>> maps = new ArrayList<>();
        maps = userMsgContentMapper.selectMsg(userMsgContent);
        // 查询点击的人
        return maps;
    }
}
