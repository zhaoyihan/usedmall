package com.shop.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.shop.domain.entity.Goods;
import com.shop.domain.entity.GoodsCar;
import com.shop.mapper.GoodsCarMapper;
import com.shop.mapper.GoodsMapper;
import com.shop.service.GoodsCarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * 购物车业务
 * @author YiHan
 **/
@Service
public class GoodsCarServiceImpl extends ServiceImpl<GoodsCarMapper, GoodsCar> implements GoodsCarService {

    @Autowired
    private GoodsCarMapper goodsCarMapper;

    @Override
    public List<Map<String,String>> getGoodsCarList(String userId) {
        return goodsCarMapper.selectGoodsCarList(userId);
    }
}
