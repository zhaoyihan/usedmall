package com.shop.service.impl;

import cn.hutool.crypto.digest.MD5;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.shop.common.AjaxResult;
import com.shop.domain.entity.User;
import com.shop.domain.vo.LoginUserVO;
import com.shop.mapper.UserMapper;
import com.shop.service.UserService;
import com.shop.util.DateUtils;
import com.shop.util.JWTUtility;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static cn.hutool.crypto.SecureUtil.md5;
import static com.shop.util.UserUtil.toLoginUserInfo;

/**
 * 用户业务
 * @author YiHan
 **/
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public AjaxResult login(User user, String loginType) {
        if ("account".equals(loginType)) {
            QueryWrapper<User> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("userAccount", user.getUserAccount());
            User countUser = userMapper.selectOne(queryWrapper);
            if (countUser == null){
                user.setCreateTime(DateUtils.getNowDate());
                user.setUserPassword(md5(user.getUserPassword()));
                userMapper.insert(user);
                QueryWrapper<User> newUserQueryWrapper = new QueryWrapper<>();
                newUserQueryWrapper.eq("userAccount", user.getUserAccount());
                User newUser = userMapper.selectOne(newUserQueryWrapper);
                return AjaxResult.success(toLoginUserInfo(newUser));
            }
            if ("1".equals(countUser.getIsDelete())){
                return AjaxResult.error("该账号被停用,请联系关联处理");
            }
            queryWrapper.eq("userPassword", md5(user.getUserPassword()));
            User passUser = userMapper.selectOne(queryWrapper);
            if (passUser == null){
                return AjaxResult.error("用户名或密码错误.请重新输入");
            } else {
                return AjaxResult.success(toLoginUserInfo(passUser));
            }
        }

        if ("phone".equals(loginType)){

        }
        return AjaxResult.success();
    }

}
