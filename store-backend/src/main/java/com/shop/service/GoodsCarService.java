package com.shop.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.shop.domain.entity.GoodsCar;

import java.util.List;
import java.util.Map;

/**
 * 购物车业务
 * @author YiHan
 **/
public interface GoodsCarService extends IService<GoodsCar> {
    List<Map<String,String>> getGoodsCarList(String userId);
}
